module go-learn/hello

go 1.16

replace go-learn/greetings => ../greetings

require go-learn/greetings v0.0.0-00010101000000-000000000000
