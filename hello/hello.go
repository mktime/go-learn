package main

import (
    "fmt"

    "go-learn/greetings"
)

// https://golang.org/doc/tutorial/call-module-code

func main() {
    // Get a greeting message and print it.
    message := greetings.Hello("Gladys")
    fmt.Println(message)
}
