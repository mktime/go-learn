module go-learn

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	rsc.io/quote v1.5.2
)
