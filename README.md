###  第一个go程序
```
package main

import "fmt"

import "rsc.io/quote"

func main() {
    //fmt.Println("Hello, World!")
    fmt.Println(quoute.Go())
}
```
执行：
```
# 管理依赖
go mod init example.com/hello

# 获取依赖
go mod tidy
```

如果获取依赖报错:
```
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
```

运行当前程序：
```
D:\source\go-learn>go run .
Don't communicate by sharing memory, share memory by communicating.

```


### 参考链接
[https://golang.org/doc/tutorial/getting-started](https://golang.org/doc/tutorial/getting-started)
[https://phantomvk.github.io/2020/03/03/go/](https://phantomvk.github.io/2020/03/03/go/)